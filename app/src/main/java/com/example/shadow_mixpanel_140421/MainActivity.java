package com.example.shadow_mixpanel_140421;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    // MixPanel project token

    // Log tag
    private static final String LOG_TAG = "MixPanel Testing";
    // MixPanelAPI instance
    private MixpanelAPI mixPanel;
    // distinct id
    private static final String DISTINCT_ID = "13794";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mixPanel = MixpanelAPI.getInstance(this, PROJECT_TOKEN);

        try {
            JSONObject props = new JSONObject();
            props.put("gender", "Male");
            props.put("loggedIn", false);

//            mixPanel.track("MainActivity - onCreate called", props);
            mixPanel.track("Gender identify", props);
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Unable to add properties to JSONObject", e);
        }

        mixPanel.identify(DISTINCT_ID);

        // Ensure all future user profile properties sent from
        // the device will have the distinct_id 13793
        mixPanel.getPeople().identify(DISTINCT_ID);

        mixPanel.getPeople().set("Sign up date", "14th April, 2021");
    }

    @Override
    protected void onDestroy() {
        mixPanel.flush();
        super.onDestroy();
    }
}